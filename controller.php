<?php
include 'model.php';

/*Armazena dados do xml na variavel $xml*/
$xml = simplexml_load_file('user.xml');
/* Instancia class database */
$database = new Database();
/*Select para verificar se os registro ja foram cadastrados*/
$sql_verify = 'SELECT * FROM user WHERE email = ?';
$condition_verify = array($xml->user['email']);
$result_verify = $database->selectDB($sql_verify,$condition_verify); 

/*Condição para verificar se os dados ja foram cadastrados*/
if(empty($result_verify)){
	/*Laço de repetição dos dados do xml*/
    foreach ($xml->user as $key => $value) {		
    	/*Insert dos dados do xml no banco de dados*/
    	$sql ='INSERT INTO user (nome,sobrenome,usuario,senha,email,status) VALUES (:nome,:sobrenome,:usuario,:senha,:email,:status)';
		$params = array(
		    ':nome' => utf8_decode($value['nome']),
		    ':sobrenome' => utf8_decode($value['sobrenome']),
		    ':usuario' => $value['usuario'],
		    ':senha' => base64_encode($value['senha']),
		    ':email' => $value['email'],
		    ':status' => $value['status']
		);	
		$database->insertDB($sql,$params);
    }	
}

/*Select para retornar os dados do banco para view*/
$sql_select = 'SELECT * FROM user WHERE status = ?';
$condition = array('1');
$result = $database->selectDB($sql_select,$condition); 


?>