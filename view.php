<?php include 'controller.php'; ?>
<!DOCTYPE html>
<html>
	<head>
		<title>View</title>		
		<link rel="stylesheet" type="text/css" href="style.css">
	</head>
	<body>
		<table>
	  		<tr>
			    <th>ID</th>
			    <th>NOME</th>
			    <th>SOBRENOME</th>
			    <th>USUARIO</th>
			    <th>SENHA</th>
			    <th>EMAIL</th>
			    <th>STATUS</th>
		  	</tr>		
			<?php foreach ($result as $key => $value) { ?>
				<tr>
				    <th><?=$value['id'];?></th>
				    <th><?=$value['nome'];?></th>
				    <th><?=$value['sobrenome'];?></th>
				    <th><?=$value['usuario'];?></th>
				    <th><?=base64_decode($value['senha']);?></th>
				    <th><?=$value['email'];?></th>
				    <th><?=$value['status'];?></th>
			  	</tr>
			<?php } ?>
		</table>
	</body>
</html>