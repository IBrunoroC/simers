<?php
class database{
    
    /*Método construtor do banco de dados*/
    public function __construct(){}
    /*Evita que a classe seja clonada*/ 
    public function __clone(){}
    /*Método que destroi a conexão com banco de dados e remove da memória todas as variáveis setadas*/
    public function __destruct() {
        $this->disconnect();
        foreach ($this as $key => $value) {
            unset($this->$key);
        }
    }
     
    /*Metodos que trazem o conteudo da variavel desejada*/
    private static $dbtype   = "mysql";
    private static $host     = "localhost";
    private static $port     = "3306";
    private static $user     = "root";
    private static $password = "";
    private static $db       = "teste_simers";

    private function getDBType()  {return self::$dbtype;}
    private function getHost()    {return self::$host;}
    private function getPort()    {return self::$port;}
    private function getUser()    {return self::$user;}
    private function getPassword(){return self::$password;}
    private function getDB()      {return self::$db;}
    
    /**
    * Conexão com o banco 
    * @return object
    */
    private function connect(){
        try
        {
            $this->conexao = new PDO($this->getDBType().":host=".$this->getHost().";port=".$this->getPort().";dbname=".$this->getDB(), $this->getUser(), $this->getPassword());
        }
        catch (PDOException $i)
        {

            die("Erro: <code>" . $i->getMessage() . "</code>");
        }
         
        return ($this->conexao);

        var_dump($this->conexao);
    }
     
    /**
    * Remove conexão com o banco 
    * @return void
    */
    private function disconnect(){
        $this->conexao = null;
    }

    /**
    * Método select que retorna um array
    * @param $sql,$params
    * @return array
    */
    public function selectDB($sql,$params=null){
        $query=$this->connect()->prepare($sql);
        $query->execute($params);        
        $rs = $query->fetchAll(PDO::FETCH_ASSOC);   
        self::__destruct();
        return $rs;
    }

    /**
    * Método insert que retorna o ultimo id inserido
    * @param $sql,$params
    * @return string
    */
    public function insertDB($sql,$params=null){
        $conexao=$this->connect();
        $query=$conexao->prepare($sql);
        $query->execute($params);
        $rs = $conexao->lastInsertId();
        self::__destruct();
        return $rs;
    }

    /**
    * Método update que retorna um booleano 0 ou 1
    * @param $sql,$params
    * @return int
    */ 
    public function updateDB($sql,$params=null){
        $query=$this->connect()->prepare($sql);
        $query->execute($params);
        $rs = $query->rowCount();
        self::__destruct();
        return $rs;
    }     

    /**
    * Método delete que retorna um booleano 0 ou 1
    * @param $sql,$params
    * @return int
    */ 
    public function deleteDB($sql,$params=null){
        $query=$this->connect()->prepare($sql);
        $query->execute($params);
        $rs = $query->rowCount();
        self::__destruct();
        return $rs;
    }
}
?>